extern crate rrlearn;

use rrlearn::environment::KnownEnv;
use rrlearn::planning::policy_iteration;
use rrlearn::planning::value_iteration;

#[test]
fn policy_iteration_works() {
    let env = MatchesEnv {
        nb_matches: 12,
        nb_matches_turn: 3,
    };
    let optimal_policy = vec![1, 1, 1, 2, 3, 1, 1, 2, 3, 3, 1, 2, 3];
    let policy = policy_iteration(&env);
    assert_eq!(policy, optimal_policy);
}
#[test]
fn value_iteration_works() {
    let env = MatchesEnv {
        nb_matches: 12,
        nb_matches_turn: 3,
    };
    let optimal_policy = vec![1, 1, 1, 2, 3, 1, 1, 2, 3, 3, 1, 2, 3];
    let policy = value_iteration(&env);
    assert_eq!(policy, optimal_policy);
}

struct MatchesEnv {
    nb_matches: usize,
    nb_matches_turn: usize,
}

impl KnownEnv for MatchesEnv {
    type State = usize;
    type Action = usize;

    fn get_state(&self, s_id: usize) -> Self::State {
        s_id
    }
    fn get_action(&self, a_id: usize) -> Self::Action {
        a_id + 1
    }

    fn get_states_id(&self) -> std::ops::Range<usize> {
        0..(self.nb_matches + 1)
    }
    fn get_actions_id(&self, s_id: usize) -> Vec<usize> {
        (0..self.get_nb_actions(s_id)).collect()
    }

    fn get_nb_actions(&self, s_id: usize) -> usize {
        self.nb_matches_turn.min(self.get_state(s_id))
    }
    fn get_nb_states(&self) -> usize {
        self.nb_matches + 1
    }

    fn get_next_states_info(&self, s_id: usize, a_id: usize) -> Vec<(f32, usize, f32, bool)> {
        let opponent_state_id = s_id.saturating_sub(self.get_action(a_id));

        if self.get_state(opponent_state_id) == 0 {
            // game is lost
            return vec![(1., 0, -1., true)];
        }

        match self.get_nb_actions(opponent_state_id) {
            0 => return vec![],
            n => {
                let prob = 1. / n as f32;
                let mut next_states = Vec::with_capacity(n);

                for opponent_action_id in self.get_actions_id(opponent_state_id) {
                    let next_state_id = opponent_state_id - self.get_action(opponent_action_id);
                    let (reward, done) = if opponent_state_id == 1 {
                        (1., true)
                    } else {
                        (0., false)
                    };

                    next_states.push((prob, next_state_id, reward, done));
                }
                return next_states;
            }
        }
    }
}
