extern crate rrlearn;

use rrlearn::environment::KnownEnv;
use rrlearn::planning::value_iteration;
// use rrlearn::planning::policy_iteration;
use std::io;
use std::io::prelude::*;

fn main() {
    println!("How many matches? (usually 12)");
    let nb_matches = get_input_usize_between(1, usize::max_value());
    println!("How many matches per turn? (usually 3)");
    let nb_actions = get_input_usize_between(1, nb_matches);

    let env = MatchesEnv {
        nb_matches,
        nb_matches_turn: nb_actions,
    };

    let agent = Agent {
        policy: value_iteration(&env),
        // policy: policy_iteration(&env),
    };

    let mut game = MatchesGame::new(&agent, &Human {}, &env);

    game.play();
}

pub enum GameState {
    Finished,
    Running(usize),
}

use GameState::*;

trait Playable {
    fn reset(&mut self);
    fn display(&self);
    fn step(&mut self, turn: u32) -> &GameState;
    fn play(&mut self);
}
struct MatchesEnv {
    nb_matches: usize,
    nb_matches_turn: usize,
}

impl KnownEnv for MatchesEnv {
    type State = usize;
    type Action = usize;

    fn get_state(&self, s_id: usize) -> Self::State {
        s_id
    }
    fn get_action(&self, a_id: usize) -> Self::Action {
        a_id + 1
    }

    fn get_states_id(&self) -> std::ops::Range<usize> {
        0..(self.nb_matches + 1)
    }
    fn get_actions_id(&self, s_id: usize) -> Vec<usize> {
        (0..self.get_nb_actions(s_id)).collect()
    }

    fn get_nb_actions(&self, s_id: usize) -> usize {
        self.nb_matches_turn.min(self.get_state(s_id))
    }
    fn get_nb_states(&self) -> usize {
        self.nb_matches + 1
    }

    fn get_next_states_info(&self, s_id: usize, a_id: usize) -> Vec<(f32, usize, f32, bool)> {
        let opponent_state_id = s_id.saturating_sub(self.get_action(a_id));

        if self.get_state(opponent_state_id) == 0 {
            // game is lost
            return vec![(1., 0, -1., true)];
        }

        match self.get_nb_actions(opponent_state_id) {
            0 => return vec![],
            n => {
                let prob = 1. / n as f32;
                let mut next_states = Vec::with_capacity(n);

                for opponent_action_id in self.get_actions_id(opponent_state_id) {
                    let next_state_id = opponent_state_id - self.get_action(opponent_action_id);
                    let (reward, done) = if opponent_state_id == 1 {
                        (1., true)
                    } else {
                        (0., false)
                    };

                    next_states.push((prob, next_state_id, reward, done));
                }
                return next_states;
            }
        }
    }
}

trait Player<T> {
    fn take_action(&self, game_state: &GameState, env: &T) -> usize;
}

struct MatchesGame<'a, P1: Player<MatchesEnv> + 'a, P2: Player<MatchesEnv> + 'a> {
    player1: &'a P1,
    player2: &'a P2,
    state: GameState,
    env: &'a MatchesEnv,
}

impl<'a, P1: Player<MatchesEnv> + 'a, P2: Player<MatchesEnv> + 'a> MatchesGame<'a, P1, P2> {
    fn new(player1: &'a P1, player2: &'a P2, env: &'a MatchesEnv) -> Self {
        MatchesGame {
            player1,
            player2,
            env,
            state: Running(env.nb_matches),
        }
    }
}

impl<'a, P1: Player<MatchesEnv>, P2: Player<MatchesEnv>> Playable for MatchesGame<'a, P1, P2> {
    fn step(&mut self, turn: u32) -> &GameState {
        if let Running(nb_matches) = self.state {
            self.display();

            let action: usize;

            // Print the prompt
            print!("{}", 2 - turn % 2 /* player number */);
            io::stdout().flush().ok().expect("Could not flush stdout");

            if turn % 2 == 1 {
                action = self.player1.take_action(&self.state, &self.env);
            } else {
                action = self.player2.take_action(&self.state, &self.env);
            }

            if nb_matches.saturating_sub(action) == 0 {
                println!(
                    "Player {} wins !",
                    turn % 2 + 1 /* next player number */
                );
                self.state = Finished;
            } else {
                self.state = Running(nb_matches - action);
            }
            return &self.state;
        }

        panic!("The game is finished !");
    }
    fn display(&self) {
        // Display the number of matches
        if let Running(nb_matches) = self.state {
            println!("{:|<1$} ({})", "", nb_matches);
        } else {
            println!("The game is over !");
        }
    }
    fn play(&mut self) {
        let mut turn = 1;
        loop {
            match self.step(turn) {
                Finished => break,
                _ => (),
            }
            turn += 1;
        }
    }
    fn reset(&mut self) {
        self.state = Running(self.env.nb_matches);
    }
}

struct Human;

struct Agent {
    policy: Vec<usize>,
}

impl<T> Player<T> for Agent {
    fn take_action(&self, state: &GameState, _env: &T) -> usize {
        if let &Running(nb_matches) = state {
            let action = self.policy[nb_matches];
            println!("> {}", action);
            return action;
        }
        panic!("The game is over !");
    }
}

impl Player<MatchesEnv> for Human {
    fn take_action(&self, state: &GameState, env: &MatchesEnv) -> usize {
        if let &Running(nb_matches) = state {
            return get_input_usize_between(1, env.nb_matches_turn.min(nb_matches));
        }
        panic!("The game is over !");
    }
}

fn get_input_usize_between(a: usize, b: usize) -> usize {
    // ask for a number in [a; b]
    let mut input = String::new();
    loop {
        print!("> ");
        io::stdout().flush().ok().expect("Could not flush stdout");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        match input.trim().parse() {
            Ok(num) if a <= num && num <= b => return num,
            _ => {
                input.clear();
            }
        };
    }
}
