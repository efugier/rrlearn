pub fn f32_max(vec: &Vec<f32>) -> f32 {
    return vec.iter().cloned().fold(-1. / 0., f32::max);
}

pub fn f32_min(vec: &Vec<f32>) -> f32 {
    return vec.iter().cloned().fold(1. / 0., f32::min);
}

pub fn f32_argmax(vec: &[f32]) -> usize {
    return vec
        .iter()
        .cloned()
        .enumerate()
        .fold(
            (0usize, -1. / 0.),
            |(j, y), (i, x)| {
                if x > y {
                    (i, x)
                } else {
                    (j, y)
                }
            },
        )
        .0;
}

pub fn f32_argmin(vec: &[f32]) -> usize {
    return vec
        .iter()
        .cloned()
        .enumerate()
        .fold(
            (0usize, 1. / 0.),
            |(j, y), (i, x)| {
                if x < y {
                    (i, x)
                } else {
                    (j, y)
                }
            },
        )
        .0;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn f32_max_correctness() {
        assert_eq!(3., f32_max(&vec![1., 3., -2.]));
    }
    #[test]
    fn f32_min_correctness() {
        assert_eq!(-3., f32_min(&vec![-1., -3., 2.]));
    }
    #[test]
    fn f32_argmax_correctness() {
        assert_eq!(1, f32_argmax(&vec![1., 3., -2.]));
        assert_eq!(0, f32_argmax(&vec![1., 3., -2.][1..]));
    }
    #[test]
    fn f32_argmin_correctness() {
        assert_eq!(1, f32_argmin(&vec![-1., -3., 2.]));
        assert_eq!(0, f32_argmin(&vec![-1., -3., 2.][1..]));
    }
}
