use environment::KnownEnv;
use utils::{f32_argmax, f32_max};

pub fn value_iteration<Env>(env: &Env) -> Vec<Env::Action>
where
    Env: KnownEnv,
{
    let gamma = 0.9;
    let theta = 1e-10;

    let mut state_values: Vec<f32> = vec![0.; env.get_nb_states()];
    let mut action_values: Vec<Vec<f32>> = Vec::with_capacity(env.get_nb_states());

    for s_id in env.get_states_id() {
        action_values.push(vec![0.; env.get_nb_actions(s_id)]);
    }

    loop {
        let mut max_delta = 0.;

        for s_id in env.get_states_id() {
            let old_value = state_values[s_id];

            for (a_pos, &a_id) in env.get_actions_id(s_id).iter().enumerate() {
                action_values[s_id][a_id] = 0.;
                for (prob, new_state, reward, done) in env.get_next_states_info(s_id, a_id) {
                    let value;
                    if done {
                        value = reward;
                    } else {
                        value = gamma * state_values[new_state];
                    }

                    action_values[s_id][a_pos] += prob * value;
                }
            }
            state_values[s_id] = f32_max(&action_values[s_id]);
            max_delta = (old_value - state_values[s_id]).abs().max(max_delta);
        }

        if max_delta < theta {
            break;
        }
        println!("state_values: {:?}", state_values);
    }

    let mut pi = Vec::with_capacity(env.get_nb_states());

    for s_id in env.get_states_id() {
        let actions = env.get_actions_id(s_id);
        if actions.len() > 0 {
            pi.push(actions[f32_argmax(&action_values[s_id])]);
        } else {
            pi.push(0);
        }
    }

    println!("policy: {:?}", pi);
    return pi.iter().map(|&a_id| env.get_action(a_id)).collect();
}
