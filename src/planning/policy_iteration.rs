use environment::KnownEnv;
use utils::f32_argmax;

fn policy_evaluation<Env>(pi: &Vec<usize>, env: &Env) -> Vec<f32>
where
    Env: KnownEnv,
{
    let gamma = 0.9;
    let theta = 1e-10;
    let mut state_values = vec![0f32; pi.len()];

    loop {
        let mut max_delta = 0.;
        let old_state_values = state_values.clone();

        for s_id in env.get_states_id() {
            state_values[s_id] = 0.;

            for (prob, new_state, reward, done) in env.get_next_states_info(s_id, pi[s_id]) {
                let value;
                if done {
                    value = reward;
                } else {
                    value = gamma * old_state_values[new_state];
                }

                state_values[s_id] += prob * value
            }

            max_delta = (old_state_values[s_id] - state_values[s_id])
                .abs()
                .max(max_delta);
        }

        if max_delta < theta {
            break;
        }
    }
    println!("state_values: {:?}", state_values);
    return state_values;
}

fn policy_improvement<Env>(pi: &mut Vec<usize>, state_values: &Vec<f32>, env: &Env)
where
    Env: KnownEnv,
{
    let gamma = 0.9;

    for s_id in env.get_states_id() {
        let actions = env.get_actions_id(s_id);
        if actions.len() == 0 {
            continue;
        }
        let mut action_values = vec![0.; actions.len()];

        for (a_pos, &a_id) in actions.iter().enumerate() {
            for (prob, new_state, reward, done) in env.get_next_states_info(s_id, a_id) {
                let value;
                if done {
                    value = reward;
                } else {
                    value = gamma * state_values[new_state];
                }

                action_values[a_pos] += prob * value;
            }
        }
        pi[s_id] = actions[f32_argmax(&action_values)];
    }
    println!("policy: {:?}", pi);
}

pub fn policy_iteration<Env>(env: &Env) -> Vec<Env::Action>
where
    Env: KnownEnv,
{
    let mut pi = vec![0; env.get_nb_states()];
    let mut state_values;

    loop {
        state_values = policy_evaluation(&pi, env);
        let old_pi = pi.clone();
        policy_improvement(&mut pi, &state_values, env);
        if old_pi == pi {
            return pi.iter().map(|&a_id| env.get_action(a_id)).collect();
        }
    }
}
