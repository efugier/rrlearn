/// states and actions ids shall always be returned in the same order
pub trait KnownEnv {
    type State;
    type Action;

    /// return the actual state associated with the given id
    fn get_state(&self, s_id: usize) -> Self::State;
    /// return the actual action associated with the given id
    fn get_action(&self, a_id: usize) -> Self::Action;

    /// return the id of every state, always in the same order
    fn get_states_id(&self) -> std::ops::Range<usize>;
    /// return the id of the available action in state `s`, always in the same order
    fn get_actions_id(&self, s_id: usize) -> Vec<usize>;

    /// return the number of states in the whole environment
    fn get_nb_states(&self) -> usize;
    /// return the number of action available from state `s`
    fn get_nb_actions(&self, s_id: usize) -> usize;

    /// for a given action `a` in a given state `s`,
    /// return a vec of (proba, next_state_id, reward, done)
    fn get_next_states_info(&self, s_id: usize, a_id: usize) -> Vec<(f32, usize, f32, bool)>;
}
